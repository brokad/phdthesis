{-# LANGUAGE OverloadedStrings #-}

module Main where

import Text.LaTeX.Base.Parser (parseLaTeXFile)
import Text.LaTeX.Base.Render (renderFile)
import Text.LaTeX.Base (Monoid (..), (<>))
import Text.LaTeX.Base.Syntax (LaTeX (..))
import qualified Data.Text.IO as TIO

import Debug.Trace

import Lib (makeTeX)


main :: IO ()
main = do

  master <- TIO.readFile "thesis/master.md"
  preamble <- parseLaTeXFile "thesis/preamble.tex"

  case preamble of
    Left err -> putStrLn (show err)
    Right p ->
      let cdoc = p <> TeXEnv "document" [] (makeTeX master)
      in renderFile "thesis/out.tex" cdoc

  
