{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}

module Lib
    ( makeTeX
    ) where

import Text.LaTeX.Base.Texy (Texy (..))
import Text.LaTeX.Base.Class (fromLaTeX)
import Text.LaTeX.Base.Syntax (
  LaTeX (..),
  (<>),
  TeXArg (..),
  MathType (..))
import Text.LaTeX.Base.Commands (
  par,
  label,
  ref,
  cite,
  emph,
  itemize, item)
import Text.LaTeX.Base.Parser (parseLaTeX)
import Text.Parsec (
  Parsec,
  (<?>),
  Stream (..),
  ParseError,
  runParser,
  between,
  skipMany1, skipMany,
  many1,
  option, optionMaybe, optional,
  try,
  manyTill,
  eof,
  count,
  lookAhead,
  notFollowedBy,
  sepEndBy,
  unexpected,
  choice)
import qualified Text.Parsec as P
import Text.Parsec.Char (
  noneOf, oneOf,
  space, spaces,
  endOfLine,
  char, anyChar, string,
  alphaNum)
import Control.Monad.Trans.State.Lazy (State)
import Control.Monad.Error.Class (MonadError (..), liftEither)
import qualified Data.Text as T
import Control.Applicative (Alternative (..), Applicative (..), ZipList (..))
import Data.String (IsString (..))
import Data.Foldable (Foldable (..))
import Data.Maybe (fromMaybe)

import Debug.Trace (trace)


type DocuState = State [LaTeX] LaTeX
type Parser = Parsec T.Text DocuState
type EulerMdParser = Parser LaTeX

type Tag = String
type Content = String

data Reference = Ref Tag
               | Cite Tag deriving Show

data Label = Label Tag

data TaggedBlock = TaggedBlock Tag Content

instance Texy TaggedBlock where
  texy (TaggedBlock t c) =
    let parsedContent = case parseLaTeX (T.pack c) of
                          Left err -> TeXRaw . T.pack $ show err
                          Right l -> l
        textag = TeXRaw . T.pack $ t
    in fromLaTeX $ TeXEnv "TaggedBlock" [(FixArg textag)] parsedContent         
  
instance Texy Label where
  texy (Label t) = fromLaTeX . label . TeXRaw . T.pack $ t

instance Texy Reference where
  texy (Ref t) = fromLaTeX . ref . TeXRaw . T.pack $ t
  texy (Cite t) = fromLaTeX . cite . TeXRaw . T.pack $ t

newtype Presentation a = Presentation { unPresent :: (Presentable a) => a -> LaTeX }

class Texy p => Presentable p where
  present :: p -> LaTeX

instance Presentable Reference where
  present ref =
    let (before, after) =
          TeXRaw <$> case ref of
                       Ref _ -> ("(", ")")
                       Cite _ -> ("", "")
    in before <> texy ref <> after


zeroState = return $ TeXRaw ""


argFromString :: String -> TeXArg
argFromString = FixArg . TeXRaw . T.pack

sharp = char '#'

partFromPrefix :: String -> String
partFromPrefix prefix = case prefix of
  "#" -> "chapter"
  "##" -> "section"
  "###" -> "subsection"
  "####" -> "subsubsection"
  _ -> "paragraph"

partDecl :: EulerMdParser
partDecl = do
  skipMany endOfLine
  prefix <- many1 sharp
  -- td: explicit label
  spaces
  head <- manyTill anyChar (lookAhead newBlock)
  return $ TeXComm (partFromPrefix prefix) [argFromString head]

parenthesized :: Parser a -> Parser a
parenthesized = between (char '(') (char ')')

emphasized :: Parser a -> Parser a
emphasized = between (char '*') (char '*')

doubledollars = string "$$"

justOrEmpty :: Maybe LaTeX -> LaTeX
justOrEmpty = fromMaybe TeXEmpty

displayMath :: Parser LaTeX
displayMath = do
  doubledollars
  l <- option TeXEmpty (texy <$> parseLabel)
  content <- manyTill anyChar doubledollars
  -- td: switch envname if content has "\\" or "&"
  let envname = if '&' `elem` content
                then if l == TeXEmpty
                     then "align*"
                     else "align"
                else if l == TeXEmpty
                     then "displaymath"
                     else "equation"
  return $ TeXEnv envname [] (l <> (TeXRaw . T.pack $ content))

parseLaTeX' :: String -> Parser LaTeX
parseLaTeX' t = do
  either (\e -> unexpected (show e)) return (parseLaTeX . T.pack $ t)

parsePresentation :: (Texy a) => Parser (Presentation a)
parsePresentation = do
  user <- parseLaTeX' =<< many (noneOf "?]")
  many1 (char '?')
  return $ Presentation (\a -> case user of
                                 TeXEmpty -> present a
                                 _ -> user <> texy a)

parseReference :: Parser Reference
parseReference = do
  ref <- (Cite <$> (char '@' *> (many1 alphaNum)))
         <|> (Ref <$> (many1 $ alphaNum <|> oneOf "-_"))
  optional weakComma
  return ref

weakComma :: Parser ()
weakComma = try $ do
  spaces
  char ','
  spaces

parseLabel :: Parser Label
parseLabel = Label <$> parenthesized (many1 $ alphaNum <|> oneOf "-_")

href :: Parser LaTeX
href = do
  ps <- ZipList
        <$> (string "[" *> manyTill (unPresent <$> parsePresentation) (char ']'))
  refs <- ZipList
          <$> (string "(" *> manyTill parseReference (char ')'))
  return $ fold (($) <$> ps <*> refs)

(<+>) :: (Monoid m) => Parser m -> Parser m -> Parser m
(<+>) = liftA2 (<>)

dollarEnv :: Parser LaTeX
dollarEnv = parseLaTeX' =<< 
  string "$" <+> many (noneOf "$") <+> string "$"

emphed :: Parser LaTeX
emphed = emph <$> (parseLaTeX' =<< do
  char '*'
  manyTill (noneOf "*") (char '*'))
  
special :: Parser LaTeX
special = try href
          <|> try emphed
          <|> try dollarEnv

breakers :: String
breakers = ".,:;?! "

word :: Parser LaTeX
word = TeXRaw . T.pack <$>
  many (oneOf breakers)
  <> many (noneOf $ breakers ++ "\r\n")
  <> many (oneOf breakers)

segment :: Parser LaTeX
segment = special <|> word

tagged :: Parser TaggedBlock
tagged = TaggedBlock
         <$> manyTill alphaNum (char '|')
         <*> manyTill anyChar (lookAhead newBlock)

newBlock :: Parser String
newBlock = try $ count 2 endOfLine

displayed :: Parser LaTeX
displayed = do
  endOfLine
  char '*'
  envname <- many (noneOf " .(")
  spaces
  envlabel <- option TeXEmpty (texy <$> parseLabel)
  string ".*"
  spaces
  nested <- manyTill block end
  return $ TeXEnv envname [] (envlabel <> fold nested)
    where end = lookAhead $ try $ count 3 endOfLine

block :: Parser LaTeX
block = do
  optional newBlock
  try didascalia
    <|> try list
    <|> paragraph

laTeXEnv :: Parser LaTeX
laTeXEnv = do
  lookAhead (string "\\begin")
  TeXRaw . T.pack <$> manyTill anyChar (lookAhead newBlock)

didascalia :: Parser LaTeX
didascalia = try partDecl
             <|> try displayed
             <|> try displayMath
             <|> try laTeXEnv

list :: Parser LaTeX
list = itemize . fold <$> many1 (try item')
  where item' = (pure $ item (Just $ TeXRaw "--")) <+> do
          optional newBlock
          char '-'
          fold <$> manyTill segment (lookAhead newBlock)

displayedBlockEnd :: Parser LaTeX
displayedBlockEnd = TeXRaw . T.pack <$> do
  endOfLine
  return ""

newPar :: Parser LaTeX
newPar = do
  mnext <- optionMaybe $ lookAhead $ try $ newBlock *> (try didascalia
                                                        <|> try list
                                                        <|> try displayedBlockEnd)
  case mnext of
    Nothing -> return par
    _ -> return TeXEmpty

trace' :: (Monad m, Show a) => m a -> m a
trace' m = do
  mo <- m
  trace (show mo) (return mo)

paragraph :: Parser LaTeX
paragraph = do
  skipMany endOfLine
  mcontent <- fold <$> manyTill segment (lookAhead newBlock)
  npar <- newPar
  return $ mcontent <> npar

parseAll :: Parser [LaTeX]
parseAll = P.manyTill block (lookAhead $ try $ do { skipMany endOfLine; eof })

makeTeX :: T.Text -> LaTeX
makeTeX s = case runParser parseAll zeroState "emd/LaTeX Transpiler" s of
  Left p -> TeXRaw $ trace (show p) "Uh oh. Unhandled error (for now): "
  Right l -> fold l
    
