
build:
	stack --nix build --resolver lts-11.21
compile:
	stack exec eulermd-exe
pdflatex:
	docker run -it --rm -v ${PWD}/thesis:/doc/:rw thomasweise/texlive pdflatex.sh /doc/out.tex

all: build compile pdflatex
